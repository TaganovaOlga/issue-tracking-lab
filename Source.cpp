#include <iostream>
#include <clocale>
#include <iomanip>
using namespace std;

int NumberOfTasks()
{
	setlocale(LC_CTYPE, "rus");
	wcout << L"������� ���������� �����" << endl;
	int count;
	wcin >> count;
	count = count * 2;
	return count;
}


int* Duration(int*longtime, int count) //� �������� ��������� ������������ ����������, � ������� � �������� ��������� ������������ ���������� �����
{                                     
	setlocale(LC_CTYPE, "rus");

	wchar_t str[7];
	for (int i = 0; i < count; i += 2)
	{
		wcin.get();
		wcout << L"������� ����������������� ������ � ������� ��h��m" << endl;
		wcin.getline(str, 7);
		wcout << endl;
		longtime[i] = (((int)(str[0]) - 48) * 10 + ((int)(str[1]) - 48));
		longtime[i + 1] = (((int)(str[3]) - 48) * 10 + (int)(str[4]) - 48);

		//��������
		if (longtime[i] > 60 || longtime[i + 1] > 60 || longtime[i] < 0 || longtime[i + 1] < 0
			|| ((longtime[i] == 0) && (longtime[i + 1] == 0)) || str[2]!='h' || str[5]!='m' || !isdigit(str[0]) || !isdigit(str[1]) 
			|| !isdigit(str[3]) || !isdigit(str[4]))
		{
			wcout << L"������������ ������� �� �����, �������� ��� ���" << endl;
			i -= 2;
			continue;
		}
	}
	return longtime; 
}



void Opportunity(int*longtime, int count) //� �������� ��������� ������������ ����������, � ������� � �������� ��������� ������������ ���������� �����
{
	float hour = 0;
	for (int i = 0; i < count; i += 2)
	{
		hour += longtime[i];
	}
	int minutes = 0;
	for (int i = 1; i < count; i += 2)
	{
		minutes += longtime[i];
	}
	hour += (minutes / 60);
	if (hour <= 40)
	{
		wcout << L"��������� ������ ��������� ��� ������"<<endl;
	}
	else
	{
		wcout << L"��������� �� ������ ��������� ��� ������" << endl;
	}
}



void FindMinDuration(int*longtime, int count)
{
	int minlong = 7000;
	int minnumber;
	for (int i = 0; i < count; i += 2)
	{
		if (minlong > (60 * longtime[i] + longtime[i + 1]))
		{
			minlong = (60 * longtime[i] + longtime[i + 1]);
			minnumber = (i / 2) + 1;
		}
	}
	float result;
	int j;
	j = (minnumber - 1) * 2;
	result = (60 *(float)longtime[j] + (float)longtime[j + 1]) / 60;
	wcout << L"����� ����� �������� ������ - " << minnumber << endl;
	wcout << L"����������������� - " << fixed << setprecision(2)<< result << endl;
}


void FindMaxDuration(int*longtime, int count)
{
	int maxlong = -1;
	int maxnumber;
	for (int i = 0; i < count; i+=2)
	{
		if (maxlong < (60 * longtime[i] + longtime[i + 1]))
		{
			maxlong = (60 * longtime[i] + longtime[i + 1]);
			maxnumber = (i / 2) + 1;
		}
	}
	float result;
	int j;
	j = (maxnumber - 1) * 2;
	result = (60 * (float)longtime[j] + (float)longtime[j + 1])/60;
	wcout << L"����� ����� ��������������� ������ - " << maxnumber << endl;
	wcout << L"����������������� - " << fixed << setprecision(2) << result << endl;
}



void wmain()
{
	int Numbers = NumberOfTasks();//����� ������ ���������� �����
	int*mas = new int[Numbers];//������� ������
	 Duration(mas, Numbers);
	 Opportunity(mas, Numbers);//
	 FindMaxDuration(mas,  Numbers);
	 wcout << endl;
	 FindMinDuration(mas,  Numbers);

	system("pause");
}

